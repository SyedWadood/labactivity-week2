#include <iostream>

int main()
{
  double price;
  double tip_per;
  double tip;
  double total_amt;
  std::cout << "Please Enter price(pounds) \n";
  std::cin >> price;
  std::cout << "Please Enter tip percentage \n";
  std::cin >> tip_per;
  
  tip = price * (tip_per / 100);
  total_amt = price + tip;
  
  std::cout<< "The tip  is " << tip << "\n";
  std::cout<< "The total amount to pay is " << total_amt << "\n"; 
  return 0;

}  
  
    
